
import os
import re

from collections import namedtuple
from operator import itemgetter, attrgetter, methodcaller

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

from Levenshtein import distance

from .forms import SearchForm

class Person():

    pattern = None

    def __init__(self, name):
        self.name = name
        self.distance = distance(self.name, self.pattern)
        self.starts_with_pattern = bool(re.match('^' + self.pattern, self.name))


def search(request):
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            cwd = os.path.dirname(os.path.abspath(__file__))
            with open(os.path.join(cwd, 'names.csv')) as file_obj:
                Person.pattern = form.cleaned_data['name']
                names = file_obj.readlines()
                results = [
                    Person(name)
                    for name
                    in names
                ]
                results.sort(key=attrgetter('distance', 'name'))
                results.sort(key=attrgetter('starts_with_pattern'), reverse=True)
                
            return render(request, 'view-results.html', {'results': results[:25], 'form': form})
    else:
        form = SearchForm()
    return render(request, 'search.html', {'form': form})
