# Full-Stack Software Engineer Test#

As part of Quantaverse's interview process, the prospect candidate is asked to solve the following task:

* Build a web app that allows a user to query a person's name and display a table with the search result(s) and the matching score(s). Your search universe must be the name dataset found in /data/names.txt. This text file contains 73k+ names of different nationalities separated by pipes ('|'). Feel free to use your preferred programming languages.

Start by cloning this repo and creating a branch named after your first name. 
Commit your solution and documentation on how to compile/access your application (as many times as necessary) to that branch.
After completion, go ahead and issue a Pull Request through BitBucket.

You will be evaluated based on cleanness, clarity, performance, and innovation of your solution.

Should you have questions, do not hesitate to contact us (Leandro - lloss@quantaverse.net).

Good luck!
Leandro